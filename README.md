DatePickerPreference
==================
[ ![Download](https://api.bintray.com/packages/amlang/maven/DatePickerPreference/images/download.svg?version=1.0.2) ](https://bintray.com/amlang/maven/DatePickerPreference/1.0.2/link)
Via a settings dialog a date can be set with the datepicker.

![screenshot_1546632288.png](https://gitlab.com/amlang/datepickerpreference/raw/master/screenshot_1546632288.png "Settings 2")

1) I suppose, you already have jcenter() in your build.gradle. So, just add a dependency:
```
    implementation "com.amlinteractive:datepickerpreference:1.0.2"
```

2) Declare the view in your XML:
```xml
 <com.amlinteractive.datepickerpreference.DatePickerPreference
            android:key="birthday"
            android:title="Birthday"
            android:summary="When's your birthday?"
            android:dialogMessage="When's your birthday?"
            android:negativeButtonText="@null"
            android:positiveButtonText="@android:string/ok"/>
```