package com.amlinteractive.datepickerpreference

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.preference.DialogPreference
import android.util.AttributeSet
import android.view.View
import android.widget.DatePicker
import android.widget.TextView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class DatePickerPreference(context: Context?, attrs: AttributeSet?) : DialogPreference(context, attrs) {
    private var dateString: String = formatter().format(Date())
    private var dismissOnChange = true

    init {
        dialogLayoutResource = R.layout.date_picker_preference
        context?.theme?.obtainStyledAttributes(attrs, R.styleable.DatePickerPreference, 0, 0)?.apply {
            try {
                dismissOnChange = getBoolean(R.styleable.DatePickerPreference_dismissOnChange, false)
            } finally {
                recycle()
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun formatter(): SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

    private fun summaryFormatter(): DateFormat = SimpleDateFormat.getDateInstance()

    override fun onCreateDialogView(): View {
        val dialogView = super.onCreateDialogView()
        dialogView.findViewById<TextView>(R.id.datePickerDialogMessage).text = dialogMessage
        val datePicker = dialogView.findViewById<DatePicker>(R.id.datePicker)
        val calendar = Calendar.getInstance()
        calendar.time = formatter().parse(dateString)
        datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        { _: DatePicker, newYear: Int, newMonth: Int, newDayOfMonth: Int ->
            val selected = GregorianCalendar(newYear, newMonth, newDayOfMonth)
            dateString = formatter().format(selected.time)
            if (dismissOnChange)
                this.dialog.dismiss()
        }
        return dialogView
    }

    override fun onGetDefaultValue(a: TypedArray?, index: Int) = a?.getString(index)

    override fun onDialogClosed(positiveResult: Boolean) {
        super.onDialogClosed(positiveResult)
        persistString(dateString)
        summary = summaryFormatter().format(formatter().parse(dateString))
    }

    override fun onSetInitialValue(restorePersistedValue: Boolean, defaultValue: Any?) {
        super.onSetInitialValue(restorePersistedValue, defaultValue)
        if (restorePersistedValue) {
            dateString = getPersistedString((defaultValue ?: dateString) as String)
            summary = summaryFormatter().format(formatter().parse(dateString))
        } else {
            dateString = defaultValue as String
            if (shouldPersist()) {
                persistString(dateString)
                summary = summaryFormatter().format(formatter().parse(dateString))
            }
        }

    }
}